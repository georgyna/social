import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
  ADD_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST,
  DELETE_POST
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

const deletePostAction = postId => ({
  type: DELETE_POST,
  postId
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

export const likePost = postId => async (dispatch, getRootState) => {
  const { isLike } = await postService.getPostReaction(postId);
  let dislikeDiff = 0;
  if (isLike === false) { // if user disliked post before remove dislike
    dislikeDiff = -1;
  }

  const { id } = await postService.reactToPost(postId, true);
  const { posts: { posts, expandedPost } } = getRootState();
  const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed

  const mapLikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + diff, // diff is taken from the current closure
    dislikeCount: Number(post.dislikeCount) + dislikeDiff
  });

  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }
};

export const dislikePost = postId => async (dispatch, getRootState) => {
  const { isLike } = await postService.getPostReaction(postId);
  let likeDiff = 0;
  if (isLike === true) { // if user liked post before remove like
    likeDiff = -1;
  }

  const { id } = await postService.reactToPost(postId, false);
  const diff = id ? 1 : -1; // if ID exists then the post was disliked, otherwise - dislike was removed

  const mapDislikes = post => ({
    ...post,
    dislikeCount: Number(post.dislikeCount) + diff, // diff is taken from the current closure
    likeCount: Number(post.likeCount) + likeDiff
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapDislikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapDislikes(expandedPost)));
  }
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const editPost = (postId, editedMessage) => async (dispatch, getRootState) => {
  const { id, body } = await postService.editPost(postId, editedMessage);
  const mapPosts = post => ({
    ...post,
    body
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== id
    ? post
    : mapPosts(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === id) {
    dispatch(setExpandedPostAction(mapPosts(expandedPost)));
  }
};

export const deletePost = postId => async dispatch => {
  const { code } = await postService.deletePost(postId);
  if (code === 1) {
    dispatch(deletePostAction(postId));
  }
};

export const editComment = (commentId, editedMessage) => async (dispatch, getRootState) => {
  const { body, id, postId } = await commentService.editComment(commentId, editedMessage);
  const mapComments = post => ({
    ...post,
    comments: [...post.comments || []].map(comment => (comment.id === id ? { ...comment, body } : comment))
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};
