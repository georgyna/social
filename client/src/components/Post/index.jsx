import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon, Form } from 'semantic-ui-react';
import moment from 'moment';

import styles from './styles.module.scss';

const Post = ({ post, likePost, dislikePost, toggleExpandedPost, sharePost, editPost, deletePost, userId }) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;
  const date = moment(createdAt).fromNow();

  const [postBody, setPostBody] = useState(body);
  const [editMode, setEditMode] = useState(false);

  const handleEditPost = ({ target }) => {
    editPost(id, target.value);
    setEditMode(false);
  };

  const openEditor = () => setEditMode(true);
  const postBodyEl = editMode
    ? (
      <Form>
        <Form.TextArea
          name="body"
          value={postBody}
          placeholder="Enter your message"
          onChange={ev => setPostBody(ev.target.value)}
          onBlur={handleEditPost}
          autoFocus
        />
      </Form>
    )
    : body;

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>
          {postBodyEl}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id)}>
          <Icon name="thumbs up" />
          {likeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikePost(id)}>
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
        {user.id === userId ? (
          <>
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={openEditor}>
              <Icon name="edit" />
            </Label>
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => deletePost(id)}>
              <Icon name="trash alternate" />
            </Label>
          </>
        ) : null}
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  editPost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired
};

export default Post;
