import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Form } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

const Comment = ({ comment: { body, createdAt, user, id }, userId, editComment }) => {
  const [commentBody, setCommentBody] = useState(body);
  const [editMode, setEditMode] = useState(false);

  const openEditor = () => setEditMode(true);

  const handleEdit = ({ target }) => {
    editComment(id, target.value);
    setEditMode(false);
  };

  const commentBodyEl = editMode
    ? (
      <Form>
        <Form.TextArea
          name="body"
          value={commentBody}
          placeholder="Enter your message"
          onChange={ev => setCommentBody(ev.target.value)}
          onBlur={handleEdit}
          autoFocus
        />
      </Form>
    )
    : body;

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        <CommentUI.Author as="a">
          {user.username}
        </CommentUI.Author>
        <CommentUI.Metadata>
          {moment(createdAt).fromNow()}
        </CommentUI.Metadata>
        <CommentUI.Text>
          {commentBodyEl}
        </CommentUI.Text>
        {user.id === userId ? (
          <CommentUI.Actions>
            <CommentUI.Action onClick={openEditor}>Edit</CommentUI.Action>
          </CommentUI.Actions>
        ) : null}
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  userId: PropTypes.string.isRequired,
  editComment: PropTypes.func.isRequired
};

export default Comment;
