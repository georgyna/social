import { Router } from 'express';
import * as postService from '../services/postService';

const router = Router();

router
  .get('/', (req, res, next) => postService.getPosts(req.query)
    .then(posts => res.send(posts))
    .catch(next))
  .get('/:id', (req, res, next) => postService.getPostById(req.params.id)
    .then(post => res.send(post))
    .catch(next))
  .post('/', (req, res, next) => postService.create(req.user.id, req.body)
    .then(post => {
      req.io.emit('new_post', post); // notify all users that a new post was created
      return res.send(post);
    })
    .catch(next))
  .put('/', (req, res, next) => postService.editPost(req.body)
    .then(post => res.send(post))
    .catch(next))
  .delete('/:id', (req, res, next) => postService.deletePost(req.params.id)
    .then(result => res.send({ code: result }))
    .catch(next))
  .put('/react', (req, res, next) => postService.setReaction(req.user.id, req.body)
    .then(reaction => {
      if (reaction.post && (reaction.post.userId !== req.user.id)) {
        // notify a user if someone (not himself) liked his post
        let event = 'like';
        let reactionMessage = 'Your post was liked!';
        if (reaction.isLike === false) {
          event = 'dislike';
          reactionMessage = 'Your post was disliked!';
        }
        req.io.to(reaction.post.userId).emit(event, reactionMessage);
      }
      return res.send(reaction);
    })
    .catch(next))
  .get('/react/:postId', (req, res, next) => postService.getReaction(req.user.id, req.params.postId)
    .then(reaction => {
      const result = reaction ? reaction.isLike : null;
      return res.send({ isLike: result });
    })
    .catch(next));

export default router;
