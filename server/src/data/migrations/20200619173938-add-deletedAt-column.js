module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.addColumn('posts', 'deletedAt', {
    type: Sequelize.DATE,
    allowNull: true
  }),

  down: queryInterface => queryInterface.removeColumn('posts', 'deletedAt')
};
